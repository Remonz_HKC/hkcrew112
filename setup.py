from setuptools import setup
from distutils.core import setup

setup(
    name='hkcrew',
    version='0.1',
    scripts=['hkcrew'],
    author='Muhammad Quwais Safutra, Ahmad Frendi',
    author_email='neptpro112@gmail.com',
    description='a simple networking liblary and website browsing',
    long_description='the simple networking',
    url='https://bitbucket.org/Remonz_HKC/hkcrew112',
    packages=['hkcrew'],
    license='BSD',
    maintainer='HKCrew112',
    python_requires='>=2.7, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*',
    classifiers=[
        "Programming Language :: Python :: 2.7",
        "License :: Osi Approved :: BSD License",
        "Operating System :: OS Independent",
        "Topic :: Terminals",
        ],
    )
